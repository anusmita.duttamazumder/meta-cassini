# nooelint: oelint.var.mandatoryvar
#
# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# nooelint: oelint.var.override - Allow changing here
SUMMARY = "CASSINI SDK Image"
require cassini-image-base.bb

inherit features_check

REQUIRED_DISTRO_FEATURES += "cassini-sdk"
