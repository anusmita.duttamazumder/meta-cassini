#!/usr/bin/env bash
# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# Make sure the network is really up and running before continuing.
# Should only really need to use this on performance constrained systems
# (FPGA's, FVPs) where the network initialization might hit regular 
# systemd timeouts

interface="$1"

echo "Waiting for ${interface} to be online..."

# Start wait-online service in case it is dead/inactive
systemctl start "systemd-networkd-wait-online@${interface}"

# Repeat until interface is online
until
  systemctl is-active --quiet "systemd-networkd-wait-online@${interface}"
do
  # Try to fix the network
  echo "***** RESTARTING NETWORKING ******";
  systemctl restart systemd-networkd;
  systemctl start "systemd-networkd-wait-online@${interface}";

  # Print network status for debug
  networkctl status "${interface}" --no-pager --full;
  systemctl status systemd-networkd --no-pager -l -n 0;
  systemctl "status systemd-networkd-wait-online@${interface}" --no-pager -l -n 0;
  systemctl status systemd-resolved --no-pager -l -n 0;
done
echo "Network status is $(systemctl is-active "systemd-networkd-wait-online@${interface}")"

# Finally, download something to prove network is really working
wget hub.docker.com || exit $?
echo "Network is working"
