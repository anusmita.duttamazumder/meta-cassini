#!/usr/bin/env bats
#
# SPDX-FileCopyrightText: Copyright (c) 2023, Linaro Limited.
#
# SPDX-FileCopyrightText: <text>Copyright 2022 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# optee-os test(aka xtest).

if [ -z "${OPTEE_XTEST_TEST_LOG_DIR}" ]; then
    TEST_LOG_DIR="${HOME}/runtime-integration-tests-logs"
else
    TEST_LOG_DIR="${OPTEE_XTEST_TEST_LOG_DIR}"
fi

export TEST_LOG_FILE="${TEST_LOG_DIR}/optee-xtest.log"
export TEST_STDERR_FILE="${TEST_LOG_DIR}/optee-xtest-stderr.log"
export TEST_RUN_FILE="${TEST_RUNTIME_DIR}/optee-xtest.pgid"

export TEST_CLEAN_ENV="${OPTEE_XTEST_TEST_CLEAN_ENV:=1}"

load "${TEST_COMMON_DIR}/integration-tests-common-funcs.sh"

@test 'optee-os tests' {

    _run /usr/bin/xtest
    if [ "${status}" -ne 0 ]; then
        log "FAIL"
        return 1
    else
        log "PASS"
    fi
}
