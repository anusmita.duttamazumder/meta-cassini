# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

export TEST_COMMON_DIR = "${datadir}/runtime-integration-tests-common"
export TEST_RUNTIME_DIR = "${localstatedir}/run/cassini-integration-tests"

ENVSUBST_VARS:append = " \$TEST_COMMON_DIR\
                         \$TEST_RUNTIME_DIR "

do_install[vardeps] += "\
    TEST_COMMON_DIR \
    TEST_RUNTIME_DIR \
    "

inherit features_check
REQUIRED_DISTRO_FEATURES = "cassini-test"
