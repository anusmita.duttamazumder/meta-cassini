# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

SUMMARY = "optee-os tests (aka xtest)"
DESCRIPTION = "OP-TEE test suite is a collections of tests that are run  \
               as a linux userspace application to tests the optee interface \
               as well as the hardware capabilities that optee os exposes. "
HOMEPAGE = "https://cassini.readthedocs.io/en/latest/"

LICENSE = "MIT"
# License file is in "layers/poky/meta/files/common-licenses".
# nooelint: oelint.var.licenseremotefile
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

TEST_FILES = "\
    file://optee-xtests.bats \
    "

inherit runtime-integration-tests
require runtime-integration-tests.inc
