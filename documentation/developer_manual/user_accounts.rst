..
 # Copyright (c) 2022 Arm Limited or its affiliates. All rights reserved.
 #
 # SPDX-License-Identifier: MIT

#############
User Accounts
#############

Cassini distribution images contain the following user accounts:

 * ``root`` with administrative privileges enabled by default. The login is
   disabled if ``cassini-security`` is included in ``DISTRO_FEATURES``.

    .. note::
      When ``cassini-test`` distro feature is enabled then ``root`` login is
      enabled. Currently, running ``inline tests`` in LAVA require login as
      ``root`` to run `transfer-overlay <https://docs.lavasoftware.org/lava/actions-boot.html#transfer-overlay>`_
      commands.
 * ``cassini`` with administrative privileges enabled with ``sudo``.
 * ``user``  without administrative privileges.
 * ``test`` with administrative privileges enabled with ``sudo``. This account
   is created only if ``cassini-test`` is included in ``DISTRO_FEATURES``.

By default, each users account has disabled password. The default
administrative group name is ``sudo``. Other sudoers configuration is included
in ``meta-cassini-distro/recipes-extended/sudo/files/cassini_admin_group.in``.

If ``cassini-security`` is included in ``DISTRO_FEATURES``, each user is
prompted to a set new password on first login. For more information about
security see:
:ref:`security hardening <developer_manual/security_hardening:Security Hardening>`.

All :ref:`run-time_integration_tests_label` are executed as the ``test`` user.

A Cassini distribution image can be configured to include run-time integration
tests that validate successful configuration of the Cassini user accounts.
Details of the user accounts validation tests can be found in the
:ref:`user_accounts_tests_label` section of the
:ref:`Validation<developer_manual/validation:Validation>` documentation.
