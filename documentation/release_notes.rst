..
 # SPDX-FileCopyrightText: Copyright (c) 2023, Linaro Limited.
 #
 # SPDX-License-Identifier: MIT

######################
Release Notes - v1.0.0
######################

Known Issues or Limitations
***************************

All platforms
=============

* There is a performance issue with RSA key generation - RSA key generation is
  much slower than expected

Corstone-1000
=============

* RSA key generation fails with "PsaErrorDataInvalid" when using Parsec.

* Due to limited performance, characters may be dropped if too much data is
  sent too quickly. Consider inserting delays between characters if sending
  files or large buffers.

* Due to the limited performance, K3S is not currently supported.

N1SDP
=====

* Support for the N1SDP platform in Cassini is primarily intended for
  development, test, and demonstration of features for infrastructure
  platforms which typically use EDK2 and Trusted Services without a
  secure enclave.

* Due to a limitation of the platform hardware (it does not have enough Secure
  world RAM) Trusted Services is configured to run from Normal world RAM. This
  configuration is not compliant with the PSA specifications. Platforms
  intended for production should be configured by the platform provider to be
  compliant with the PSA specifications.

* Booting the distribution image from USB storage device means storage
  performance may be may limited by that device. If unexpected issues or test
  failures are seen when booting from USB, try using a USB device with better
  performance or alternatively try booting from a SATA storage device.

Known Test Failures
*******************

All platforms
=============

* Due to RSA key performance, the following tests take more time to complete
  than is stated  in the developer guide:

  * Parsec simple end-to-end Tests may take up to 5 hours to complete

  * OP-TEE Sanity Tests may take up to 12 hours to complete

* The following tests are expected to fail:

  * Platform Security Architecture API Tests:

    * psa-crypto-api-test 262 (Test psa_hash_suspend - SHA224)

    * psa-crypto-api-test 263 (Test psa_hash_resume - SHA224)

Corstone-1000
=============

* The following tests are expected to fail:

  * Parsec simple end-to-end Tests:

    * All RSA key tests fail with "PsaErrorDataInvalid" or
      "PsaErrorDoesNotExist"

    * The following are failures which are known to occur with the latest
      release of the Corstone-1000 platform software from meta-arm
      (CORSTONE1000-2023.06). See |Test Report for CORSTONE1000-2023.06|_.

  ..
    Disable cspell for quoted error messages
    spell-checker:disable

  * ACS BSA tests:

    * Check Cryptographic extensions (Failed on PE)

    * Check PMU Overflow signal (Invalid Interrupt ID number 0x750062)

    * Memory mapped timer check (Read-write check failed for CNTBaseN.CNTP_CTL,
      expected value 3)

    * Generate Mem Mapped SYS Timer Intr (Sys timer interrupt not received on
      34)

    * Wake from System Timer Int (Received Failsafe interrupt)

  * ACS EBBR tests:

    * UEFI Compliant - Hii protocols must be implemented

    * UEFI Compliant - Boot from network protocols must be implemented

    * UEFI Compliant - DECOMPRESS protocol must exist

    * BS.ConnectController - InterfaceTestCheckpoint14
      (F48D1C2D-1EBA-4E4C-A16D-748A01ABE6C1)

    * BS.ConnectController - InterfaceTestCheckpoint14
      (25CFFDF5-D252-4515-AF8F-D8DB68F022C3)

    * BS.ConnectController - InterfaceTestCheckpoint14
      (555913E8-BA56-4C68-80B5-A96B8A3AFCB1)

    * BS.GetNextMonotonicCount - high 32 bit increase by 1
      (F48D1C2D-1EBA-4E4C-A16D-748A01ABE6C1)

    * BS.GetNextMonotonicCount - high 32 bit increase by 1
      (E8B96EA0-6413-4947-AD1A-31EEF868A372)

    * BS.GetNextMonotonicCount - high 32 bit increase by 1
      (0EC16C83-177D-461A-9622-42508C99D966)

    * RT.SetTime - Verify year after change

    * RT.SetTime - Verify month after change

    * RT.SetTime - Verify daylight after change

    * RT.SetTime - Verify time zone after change

    * RT.SetTime - Verify year after change

    * RT.SetTime - Verify month after change

    * RT.SetTime - Verify daylight after change

    * RT.SetTime - Verify time zone after change

    * RT.UpdateCapsule - invoke UpdateCapsule with invalid ScatterGatherList

    * RT.UpdateCapsule - invoke UpdateCapsule with invalid Flags

    * RT.UpdateCapsule - invoke UpdateCapsule with invalid Flags

  * ACS SCT Tests:

    * HII_DATABASE_PROTOCOL.ExportPackageLists (ExportPackageLists() returns
      EFI_BUFFER_TOO_SMALL)

    * HII_DATABASE_PROTOCOL.SetKeyboardLayout (SetKeyboardLayout() returns
      EFI_INVALID_PARAMETER)

  * ACS FWTS Tests:

    * Validity of fw_class in UEFI ESRT Table for EBBR (The fw_classis set to
      default u-boot raw guid)

  ..
    Re-enable cspell
    spell-checker:enable

N1SDP
=====

* Some test failures are expected as the platform support is currently
  incomplete (pending further feature development):

  * Platform Security Architecture API Tests:

    * psa-api-iat-test 601 (fails with actual code 42, expected 0)

    * psa-api-ps-test 414 (fails with actual code 0, expected -134)

  ..
    Disable cspell for quoted error messages
    spell-checker:disable

  * ACS BSA tests:

    * Check Arch symmetry across PE (Timed out for PE index = 2)

    * Check for AdvSIMD and FP support (PSCI_CPU_ON: cpu already on)

    * Check PE 4KB Granule Support (PSCI_CPU_ON: cpu already on)

    * Check HW Coherence support (PSCI_CPU_ON: cpu already on)

    * Check Cryptographic extensions (PSCI_CPU_ON: cpu already on)

    * Check Little Endian support (PSCI_CPU_ON: cpu already on)

    * Check EL1 and EL0 implementation (PSCI_CPU_ON: cpu already on)

    * Check for PMU and PMU counters (PSCI_CPU_ON: cpu already on)

    * Check num of Breakpoints and type (PSCI_CPU_ON: cpu already on)

    * Check Synchronous Watchpoints (PSCI_CPU_ON: cpu already on)

    * Check CRC32 instruction support (PSCI_CPU_ON: cpu already on)

    * Check Speculation Restriction (PSCI_CPU_ON: cpu already on)

    * Check Speculative Str Bypass Safe (PSCI_CPU_ON: cpu already on)

    * Check PEs Impl CSDB,SSBB,PSSBB (PSCI_CPU_ON: cpu already on)

    * Check PEs Implement SB Barrier (PSCI_CPU_ON: cpu already on)

    * Check PE Impl CFP,DVP,CPP RCTX (PSCI_CPU_ON: cpu already on)

    * Check PPI Assignments for OS (EL0-Phy timer not mapped to PPI recommended
      range)

    * Wake from EL0 PHY Timer Int (Invalid Interrupt ID number 0xAFAFAFAF)

    * Wake from EL0 VIR Timer Int (Invalid Interrupt ID number 0xAFAFAFAF)

    * Wake from EL2 PHY Timer Int (Invalid Interrupt ID number 0xAFAFAFAF)

  * ACS EBBR tests:

    * UEFI Compliant - Boot from network protocols must be implemented

    * BS.AllocatePool - Type is EfiMaxMemoryType

  * ACS SCT Tests:

    * BS.SetWatchdogTimer (should not reset after 3.5s)

  * ACS FWTS Tests:

    * Error: uefivarinfo (initialisation failed)
