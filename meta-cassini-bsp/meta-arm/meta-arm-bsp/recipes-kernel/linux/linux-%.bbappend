# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend:cassini := "${THISDIR}:${THISDIR}/linux-yocto:"

# nooelint: oelint.vars.srcurifile - False positive
SRC_URI:append:cassini:corstone1000 = " \
    file://0001-module-Expose-module_init_layout_section.patch \
    file://0002-arm64-module-Use-module_init_layout_section.patch \
    file://0003-ARM-module-Use-module_init_layout_section.patch \
    file://extfs.cfg \
    file://cgroups.cfg \
    file://mmc.cfg \
    file://container.cfg \
    file://network.cfg \
    ${@bb.utils.contains('DISTRO_FEATURES', \
            'cassini-sdk', \
            'file://sdk.cfg', '', d)} \
    "
